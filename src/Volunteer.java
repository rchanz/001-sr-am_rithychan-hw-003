public class Volunteer extends StaffMember {

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public double pay() {
        return 0;
    }

    @Override
    public void setAddress(String address) {
        super.setAddress(address);
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String getAddress() {
        return super.getAddress();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public String toString() {
        return ("ID: " + this.getId() + "\n" + "Name: " + this.getName() + "\n"
                + "Address: " + this.getAddress() + "\n" + "Thanks!"  + "\n" + "---------------------------------");
    }
}
