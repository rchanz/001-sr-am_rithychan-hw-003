import java.util.*;
import java.util.regex.Pattern;


public class Main {
    public static Scanner sc = new Scanner(System.in);
    public static ArrayList<StaffMember> staffList = new ArrayList<>();

    public static ArrayList<StaffMember> initObjects() {
        //Examples Code
        ArrayList<StaffMember> al = new ArrayList<>();
        StaffMember sm1 = new Volunteer(1, "Jivorn", "Phnom Penh");
        StaffMember sm2 = new SalariedEmployee(2, "Chivorn", "Phnom Penh", 400, 50);
        StaffMember sm3 = new HourlyEmployee(3, "Joan", "Kampong Cham", 180, 9);

        al.add(sm1);
        al.add(sm2);
        al.add(sm3);

        return al;
    }

    //Display First Menu
    public static String show1stMenu() {
        //Primary functions menu
        String ch1;
        while (true) {
            System.out.println("---------------------------------");
            System.out.println("1. Add Employee \n2. Edit \n3. Remove \n4. Exit \n");
            System.out.print("Pick a choice (1 - 4): ");
            ch1 = sc.next();
            if (!checkChoice(ch1)) {
                System.out.println("Please input a valid choice (1 - 4)!");
            } else {
                return ch1;
            }
        }

    }

    //Display Second Menu
    public static String show2ndMenu() {
        //Show a secondary menu for Add function
        String ch2;
        while (true) {
            System.out.println("---------------------------------");
            System.out.println("1. Add Volunteer \n2. Add Salary Employee \n3. Add Hourly Employee \n4. Back \n");
            System.out.print("Pick a choice: ");
            ch2 = sc.next();
            if (!checkChoice(ch2)) {
                System.out.println("Please input a valid choice (1 - 4)!");
            } else {
                return ch2;
            }
        }

    }

    //Function for capitalizing first character
    public static String capitalizeString (String str) {
        //Validate first
        if(str == null || str.isEmpty()) {
            return str;
        }
        //Capitalize the first char
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static void addVolunteer() {
        //Adding Volunteer
        int id = 0;

        System.out.print("Input ID: ");
        String tID = sc.next();
        //Validating ID as Number only
        while (!(checkNumOnly(tID))) {
            System.out.println("Please input number only!");
            System.out.print("Input ID: ");
            tID = sc.next();
        }
        id = Integer.parseInt(tID);
        sc.nextLine();

        //Input Name
        System.out.print("Input Name: ");
        String name = sc.nextLine();

        //Input Address
        System.out.print("Input Address: ");
        String address = sc.nextLine();

        //Initialize a new object using given info then add to ArrayList
        StaffMember v = new Volunteer(id, capitalizeString(name), address);
        staffList.add(v);


    }

    public static void addSalaryEmployee() {
        String name, address;
        double salary, bonus;
        int id = 0;

        //Input ID
        System.out.print("Input ID: ");
        String tID = sc.next();
        //Validating ID as Number only
        while (!(checkNumOnly(tID))) {
            System.out.println("Please input number only!");
            System.out.print("Input ID: ");
            tID = sc.next();
        }
        id = Integer.parseInt(tID);
        sc.nextLine();

        //Input Name
        System.out.print("Input Name: ");
        name = sc.nextLine();

        //Input Address
        System.out.print("Input Address: ");
        address = sc.nextLine();

        //Input Salary
        System.out.print("Input Salary: ");
        String tSalary = sc.next();
        //Validating Salary as positive number only
        while (!(checkDouble(tSalary))) {
            System.out.println("Please input positive/valid number only!");
            System.out.print("Input Salary: ");
            tSalary = sc.next();
        }
        salary = Double.parseDouble(tSalary);

        //Input Bonus
        System.out.print("Input Bonus: ");
        String tBonus = sc.next();
        //Validating Bonus as positive number only
        while (!(checkDouble(tBonus))) {
            System.out.println("Please input positive/valid number only!");
            System.out.print("Input Bonus: ");
            tBonus = sc.next();
        }
        bonus = Double.parseDouble(tBonus);

        //Initialize a new object using given info then add to ArrayList
        StaffMember se = new SalariedEmployee(id, capitalizeString(name), address, salary, bonus);
        staffList.add(se);
    }

    public static void addHourlyEmployee() {
        int id, workedHours;
        String name, address;
        double rate;

        System.out.print("Input ID: ");
        String tID = sc.next();
        //Validating ID as Number only
        while (!(checkNumOnly(tID))) {
            System.out.println("Please input number only!");
            System.out.print("Input ID: ");
            tID = sc.next();
        }
        id = Integer.parseInt(tID);
        sc.nextLine();

        System.out.print("Input Name: ");
        name = sc.nextLine();

        System.out.print("Input Address: ");
        address = sc.nextLine();

        System.out.print("Input Worked Hours: ");
        String tHours = sc.next();
        //Validating Working Hours as number only
        while (!(checkNumOnly(tHours))) {
            System.out.println("Please input positive/valid number only!");
            System.out.print("Input Worked Hours: ");
            tHours = sc.next();
        }
        workedHours = Integer.parseInt(tHours);

        System.out.print("Input Rate: ");
        String tRate = sc.next();
        //Validating Rate as positive double only
        while (!checkDouble(tRate)) {
            System.out.println("Please input positive/valid number only!");
            System.out.print("Input Rate: ");
            tRate = sc.next();
        }
        rate = Double.parseDouble(tRate);

        //Initialize a new object using given info then add it to ArrayList
        StaffMember he = new HourlyEmployee(id, capitalizeString(name), address, workedHours, rate);
        staffList.add(he);
    }

    public static void editEmployeeInfo(int inputID) {
        boolean completed = false;
        //Iterate through the ArrayList
        for (StaffMember sm : staffList) {
            if (sm.getId() == inputID) {
                //Found the ID!
                System.out.println();
                System.out.println(sm.toString());
                System.out.println("==============EDIT INFO==============");
                if (sm instanceof Volunteer) {
                    sc.nextLine();
                    System.out.print("Input Name: ");
                    sm.setName(sc.nextLine());
                    System.out.print("Input Address: ");
                    sm.setAddress(sc.nextLine());
                    completed = true;
                }
                if (sm instanceof SalariedEmployee) {
                    sc.nextLine();
                    System.out.print("Input Name: ");
                    sm.setName(sc.nextLine());
                    System.out.print("Input Address: ");
                    sm.setAddress(sc.nextLine());
                    System.out.print("Input Salary: ");
                    ((SalariedEmployee) sm).setSalary(sc.nextDouble());
                    System.out.print("Input Bonus: ");
                    ((SalariedEmployee) sm).setBonus(sc.nextDouble());
                    completed = true;
                }
                if (sm instanceof HourlyEmployee) {
                    sc.nextLine();
                    System.out.print("Input Name: ");
                    sm.setName(sc.nextLine());
                    System.out.print("Input Address: ");
                    sm.setAddress(sc.nextLine());
                    System.out.print("Input Worked Hours: ");
                    ((HourlyEmployee) sm).setWorkedHour(sc.nextInt());
                    System.out.print("Input Rate: ");
                    ((HourlyEmployee) sm).setRate(sc.nextDouble());
                    completed = true;
                }
            }
        }
        //Status for search
        if (!completed) {
            System.out.println("Input ID cannot be found!");
        }

    }

    public static boolean checkNumOnly(String in) {
        boolean answer = Pattern.matches("\\d+", in);
        return answer;
    }

    public static boolean checkChoice(String ch) {
        boolean answer = Pattern.matches("[1-4]", ch);
        return answer;
    }

    public static boolean checkDouble(String ch) {
        //removed [-+]? because we want positive numbers only!
        boolean answer = Pattern.matches("[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?", ch);
        return answer;
    }

    public static void main(String[] args) {
        String ch1 = null, ch2 = null;
        boolean check = true;
        int inputID = 0;

        //Example objects
        staffList.addAll(initObjects());

        while (true) {
            //Sort by name in ascending order using comparator
            Collections.sort(staffList, StaffMember.nameComparator);

            //Add a line
            System.out.println();

            //Show List
            for (StaffMember sm : staffList) {
                System.out.println(sm.toString());
            }

            //Add a line
            System.out.println();

            //Menu 1
            ch1 = show1stMenu();
            switch (ch1) {
                //1. Add an employee.
                case "1":
                    while (true) {
                        //Menu 2 for adding options
                        ch2 = show2ndMenu();
                        switch (ch2) {
                            case "1":
                                System.out.println("==============INSERT INFO============");
                                addVolunteer();
                                break;
                            case "2":
                                System.out.println("==============INSERT INFO============");
                                addSalaryEmployee();
                                break;
                            case "3":
                                System.out.println("==============INSERT INFO============");
                                addHourlyEmployee();
                                break;
                            case "4":
                                break;
                        }
                        break;
                    }
                    break;

                //2. Edit employee info
                case "2":
                    System.out.println("==============EDIT INFO==============");
                    System.out.print("Enter ID to edit: ");
                    inputID = sc.nextInt();
                    editEmployeeInfo(inputID);
                    break;

                //3. Remove an employee
                case "3":
                    System.out.println("=================REMOVE==============");
                    System.out.print("ID to remove: ");
                    inputID = sc.nextInt();
                    int finalInputID = inputID;
                    for (StaffMember sm : staffList) {
                        if (sm.getId() == inputID) {
                            System.out.println(sm.toString());
                        }
                    }
                    //Such a roundabout way honestly but FeelsGoodMan, because it can work without a loop!
                    staffList.removeIf(staffMember -> staffMember.getId() == finalInputID);
                    break;

                //4. Exit
                case "4":
                    System.out.println("Goodbye!");
                    System.exit(0);
                    break;

            }
        }


    }

}
