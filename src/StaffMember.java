import java.util.Comparator;

public abstract class StaffMember {
    protected int id;
    protected String name, address;

    public StaffMember (int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String toString () {
        return ("ID: " + this.getId() + "\n" + "Name: " + this.getName() + "\n" + "Address: " + this.getAddress());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public static Comparator<StaffMember> nameComparator = new Comparator<>() {
        @Override
        public int compare(StaffMember sm1, StaffMember sm2) {
            return (int) (sm1.getName().compareTo(sm2.getName()));
        }
    };


    public abstract double pay ();
}
