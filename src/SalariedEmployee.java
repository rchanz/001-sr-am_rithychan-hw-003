import java.text.DecimalFormat;

public class SalariedEmployee extends StaffMember {
    private double salary, bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String getAddress() {
        return super.getAddress();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public double getBonus() {
        return bonus;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public void setAddress(String address) {
        super.setAddress(address);
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public static DecimalFormat df = new DecimalFormat("#.##");

    @Override
    public double pay() {
        return this.salary + this.bonus;
    }

    public String toString() {
        return ("ID: " + this.getId() + "\n" + "Name: " + this.getName() + "\n"
                + "Address: " + this.getAddress() + "\n" + "Salary: " + this.getSalary() + "\n"
                + "Bonus: " + this.getBonus() + "\n" + "Payment: " + df.format(this.pay()) + "\n" + "---------------------------------");
    }
}
