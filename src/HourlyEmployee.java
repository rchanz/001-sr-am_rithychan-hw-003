import java.text.DecimalFormat;

public class HourlyEmployee extends StaffMember {

    private int workedHour;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int workedHour, double rate) {
        super(id, name, address);
        this.workedHour = workedHour;
        this.rate = rate;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getAddress() {
        return super.getAddress();
    }

    @Override
    public int getId() {
        return super.getId();
    }

    public double getRate() {
        return rate;
    }

    public int getWorkedHour() {
        return workedHour;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public void setAddress(String address) {
        super.setAddress(address);
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setWorkedHour(int workedHour) {
        this.workedHour = workedHour;
    }

    @Override
    public double pay() {
        return this.workedHour * this.rate;
    }

    public static DecimalFormat df = new DecimalFormat("#.##");

    public String toString() {
        return ("ID: " + this.getId() + "\n" + "Name: " + this.getName() + "\n"
                + "Address: " + this.getAddress() + "\n" + "Worked Hours: " + this.getWorkedHour() + "\n"
                + "Rate: " + this.getRate() + "\n" + "Payment: " + df.format(this.pay()) + "\n" + "---------------------------------");
    }
}
